<?php

/**
 * @file
 * This module provides a form to apply for a position.
 *
 * The submissions can be viewed after aply with proper permissions.
 */

/**
 * Implements hook_permission().
 */
function position_permission() {
  return array(
    'position_apply' => array(
      'title' => t('Apply for a position'),
      'description' => t('Provides permission to use the apply form.'),
    ),
    'position_manage_applicants' => array(
      'title' => t('Manage applicants'),
      'description' => t('Provides accessibility to the individual applicants page.'),
    ),
  );
}

/**
 * Implements hook_menu().
 */
function position_menu() {
  $items = array();

  // Create menu item Apply for a position.
  $items['position/apply'] = array(
    'title' => 'Apply for a position',
    'description' => 'A form to apply for a position.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('position_form'),
    'access arguments' => array('position_apply'),
  );

  // Create a callback for use by theme_position_applicant().
  $items['position/applicants/%'] = array(
    'page callback' => 'theme_position_applicant',
    'page arguments' => array(2),
    'access arguments' => array('position_manage_applicants'),
  );

  return $items;
}

/**
 * Implements hook_form().
 */
function position_form($form, &$form_state) {

  $form['firstname'] = array(
    '#type' => 'textfield',
    '#title' => 'My first name',
    '#size' => 30,
    '#maxlength' => 255,
    '#required' => TRUE,
  );

  $form['lastname'] = array(
    '#type' => 'textfield',
    '#title' => 'My last name',
    '#size' => 30,
    '#maxlength' => 255,
    '#required' => TRUE,
  );

  $form['mail'] = array(
    '#type' => 'textfield',
    '#title' => 'My-mail address',
    '#size' => 30,
    '#maxlength' => 255,
    '#required' => TRUE,
  );

  $form['motivation'] = array(
    '#type' => 'textarea',
    '#title' => 'My motivation',
    '#size' => 30,
    '#maxlength' => 500,
    '#required' => TRUE,
  );

  $form['submit_button'] = array(
    '#type' => 'submit',
    '#value' => t('Send'),
  );

  return $form;
}

/**
 * Implements hook_form_validate().
 */
function position_form_validate($form, &$form_state) {
  if (!valid_email_address($form_state['values']['mail'])) {
    form_set_error('mail', t('The entered email address') . ' ' . $form_state['values']['mail'] . ' ' . t('is not valid.'));
  }
}

/**
 * Implements hook_form_submit().
 */
function position_form_submit($form, &$form_state) {
  $values = array(
    'firstname' => $form_state['values']['firstname'],
    'lastname' => $form_state['values']['lastname'],
    'mail' => $form_state['values']['mail'],
    'motivation' => $form_state['values']['motivation'],
  );
  $insert = db_insert('position_applicants')
    ->fields(array(
      'firstname' => $values['firstname'],
      'lastname' => $values['lastname'],
      'mail' => $values['mail'],
      'motivation' => $values['motivation'],
    ))
    ->execute();
  $form_state['redirect'] = '<front>';
  drupal_set_message(t('Thanks for applying! We will contact you soon.'));
}

/**
 * Implements hook_theme().
 */
function position_theme() {
  return array(
    'position_applicant' => array(
      'template' => 'position-applicant',
      'variables' => array(
        'results' => NULL,
      ),
    ),
  );
}

/**
 * Implements function theme_position_applicant().
 */
function theme_position_applicant($id) {
  $query = db_query("SELECT id, firstname, lastname, mail, motivation, submitted FROM {position_applicants} WHERE id = $id");
  $result = $query->fetchAll();
  foreach ($result as $record) {

    return theme('position_applicant', array(
      'results' => array(
        'id' => $record->id,
        'firstname' => $record->firstname,
        'lastname' => $record->lastname,
        'mail' => $record->mail,
        'motivation' => $record->motivation,
        'submitted' => $record->submitted,
      ),
    ));
  }
}
